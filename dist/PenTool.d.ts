import { fabric } from 'fabric';
declare module 'fabric' {
    namespace fabric {
        interface Path {
            path: Array<string | number>[];
        }
        interface Object {
            selectable?: boolean;
            __selectable?: boolean;
            hoverCursor?: string;
            __hoverCursor?: string;
        }
        interface Canvas {
            __selection?: boolean;
            off(eventName?: string | any, handler?: (e: IEvent<any>) => void): any;
        }
    }
}
interface PathPoint {
    current: fabric.Point;
    prevControl?: fabric.Point;
    nextControl?: fabric.Point;
}
declare class PenTool {
    canvas: fabric.Canvas;
    pathStyle?: fabric.IPathOptions | undefined;
    isOpen: boolean;
    private onDownCanvas;
    private onMoveCanvas;
    private onUpCanvas;
    private _activePathObj;
    private __isDestroyed;
    private isDowning;
    private isClosedPath;
    private readonly PATH_NAME;
    private readonly controlRadius;
    private _pathNodeCircles;
    private _pathNodeControls;
    private _tipsObj;
    subColor: string;
    private _pathPoints;
    private activePathPointIndex;
    constructor(canvas: fabric.Canvas, pathStyle?: fabric.IPathOptions | undefined);
    get isDestroyed(): boolean;
    set isDestroyed(val: boolean);
    private setTipsObj;
    private get pathPoints();
    private set pathPoints(value);
    drawPath(curPoints: PathPoint[]): void;
    _onDownCanvas(opt: fabric.IEvent<MouseEvent>): void;
    _onMoveCanvas(opt: fabric.IEvent<MouseEvent>): void;
    _onUpCanvas(opt: fabric.IEvent<MouseEvent>): void;
    open(): void;
    close(): void;
    destroy(): void;
    canClosePath(x: number, y: number): boolean;
    canRemovePath(x: number, y: number): boolean;
    canDonePath(x: number, y: number): boolean;
    canBreakPath(x: number, y: number, altKey?: boolean): boolean;
    makePathArr(points: PathPoint[]): (string | number)[][];
    makePathObj(paths: Array<string | number>[], pathStyle?: fabric.IPathOptions): fabric.Path;
    makeControlLine(mp: fabric.Point, sp?: fabric.Point, ep?: fabric.Point): (fabric.Circle | fabric.Line)[] | undefined;
    makeText(text: string, options?: fabric.ITextOptions): fabric.Text;
    makeCircle(point: fabric.Point, options?: fabric.ICircleOptions): fabric.Circle;
    makeLine(sp: fabric.Point, ep: fabric.Point, options?: fabric.ILineOptions): fabric.Line;
    getSymmetryPoint(midPoint: fabric.Point, controlPoint: fabric.Point): fabric.Point;
    resetObjSelectable(): void;
    complete(): void;
}
export { PenTool };
