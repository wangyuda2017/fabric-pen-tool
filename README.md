# fabric-pen-tool

fabricjs Libary pen tool, Currently supports lines and quadratic Bezier curve and cubic Bezier curve [Online Demo](https://wangyuda2017.gitee.io/fabric-pen-tool)

## Tips and Techniques

* To click open button
* After opening, the graph cannot be selected. Click to draw a straight line, click and drag the animation of a quadratic Bezier curve (cubic Bezier curve is not currently supported, supported in the next version)
* The starting point is identified by the close mark. When the mouse displays a closed circle icon at the starting point, clicking or dragging will close the graph
* Move and display the done mark at the end point, and click to complete the drawing
* Move at the end point and press the alt key to display the break flag. Click to disconnect the next control point
* You can continue selecting drawings after closing

## Installation

* Using `npm`

```bash
npm install fabric fabric-pen-tool --save
```

* Using `yarn`

```bash
yarn add fabric fabric-pen-tool
```

* Using `pnpm`

```bash
pnpm add fabric fabric-pen-tool
```

## Usage

### Initialization

```js
import { fabric } from 'fabric'
import { PenTool } from 'fabric-pen-tool'
const canvas = new fabric.Canvas('myCanvas')
const pt = new PenTool(canvas)
pt.open()
pt.close()
pt.destroy()
```

### Arguments

* canvas
  * Type: fabric.Canvas
  * Description: fabric canvas instance
  * Required: true
* pathStyle
  * Type: fabric.IPathOptions | undefined
  * Description: The style of drawn path.(eg: { stroke: '#00f', fill: '#000' })
  * Required: false

### Attributes

* isOpen: Whether to turn on the pen tool
* subColor: All auxiliary colors except for drawing paths
* isDestroyed: Whether it has been destroyed

### Methods

* open(): Turn on the pen tool
* close(): Turn off the pen tool
* destroy(): Remove all event listeners

## Used in Vue3

```html
<script setup lang="ts">
import { onMounted } from 'vue'
import { fabric } from 'fabric'
import { PenTool } from 'fabric-pen-tool'

onMounted(() => {
  const canvas = new fabric.Canvas('c')
  const pt = new PenTool(canvas)
  pt.open()
})

</script>

<template>
  <canvas width="500" height="500" id="c"></canvas>
</template>

<style>
canvas {
  border: 1px solid #000
}
</style>
```

## Used in Vue2

```html
<template>
  <canvas width="500" height="500" id="c"></canvas>
</template>

<script>
import { fabric } from "fabric"
import { PenTool } from 'fabric-pen-tool'

export default {
  mounted() {
    const canvas = new fabric.Canvas('c')
    const pt = new PenTool(canvas)
    pt.open()
    // pt.close()
    // pt.destroy()
  }
}
</script>
```

## Used in browsers

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
  <canvas width="500" height="500" id="myCanvas"></canvas>
  <script src="./fabric.min.js"></script>
  <script src="./dist/index.umd.js"></script>
  <script>
    const canvas = new fabric.Canvas('myCanvas')
    const pt = new fabricPenTool.PenTool(canvas, { originX: 'center', originY: 'center' })
    // pt.open()
    // pt.close()
    // pt.destory()
  </script>
</body>
</html>
```

## Demo picture

![demo](https://gitee.com/wangyuda2017/fabric-pen-tool/raw/master/public/fabric-pen-tool-demo1.png)
