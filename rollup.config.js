import typescript from '@rollup/plugin-typescript'
import commonjs from '@rollup/plugin-commonjs'
import nodeResolve from '@rollup/plugin-node-resolve'
import json from '@rollup/plugin-json'
import terser from '@rollup/plugin-terser'

export default [
  {
    input: 'src/index.ts',
    output: [
      {
        file: 'dist/index.js',
        format: 'cjs'
      },
      {
        file: 'dist/index.es.js',
        format: 'es'
      }
    ],
    plugins: [
      terser(),
      typescript(),
      json(),
    ],
    external: ['fabric']
  },
  {
    input: 'src/index.ts',
    output: {
      file: 'dist/index.umd.js',
      format: 'umd',
      name: 'fabricPenTool',
      globals: {
        'fabric': 'self' // 为了兼容：cjs: fabric.fabric  然而浏览器是fabric
      }
    },
    plugins: [
      terser(),
      typescript(),
      json(),
      commonjs(),
      nodeResolve()
    ],
    external: ['fabric'],
    
  }
]